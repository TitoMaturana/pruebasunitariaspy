import unittest
from com.Octal import Octal

class Test_Octal(unittest.TestCase):

    def setUp(self):
        print ("begin")
        self.hexa = Octal()
    
    def test_hex_to_octal(self):
        self.assertNotEqual(self.hexa.hex_to_octal("FFFA"), "177777", "Genero el HEXA correcto")

    def test_hex_to_octal_right(self):
        self.assertEqual(self.hexa.hex_to_octal("FFFF"), "177777", "No genero el HEXA correcto")
    
    def test_letra_a_numeros(self):
        self.assertEqual(14, self.hexa.letra_a_numeros("E"), "Convertir letra no corresponde")
    
    def test_bin_to_octal(self):
        self.assertRaises(RuntimeError, self.hexa.bin_to_octal, '00100111100101010')


if __name__ == '__main__':
    unittest.main()

